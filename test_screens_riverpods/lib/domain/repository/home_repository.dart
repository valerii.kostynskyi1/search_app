import 'package:test_screens_riverpods/data/model/repository_model.dart';

abstract class HomeRepository {
  Future<List<RepositoryModel>> getRepositoriesList({
    required int offset,
    required String query,
  });
}
