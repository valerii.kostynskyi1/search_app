import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';
import 'package:test_screens_riverpods/presentation/favorites/favorites_page.dart';
import 'package:test_screens_riverpods/presentation/home/home_screen.dart';

final GoRouter router = GoRouter(
  routes: <RouteBase>[
    GoRoute(
      path: '/',
      builder: (BuildContext context, GoRouterState state) {
        return const HomePage();
      },
      routes: <RouteBase>[
        GoRoute(
          path: 'favorite-page',
          builder: (BuildContext context, GoRouterState state) {
            return const FavoritesPage();
          },
        ),
      ],
    ),
  ],
);
