import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:get_it/get_it.dart';
import 'package:test_screens_riverpods/data/model/repository_model.dart';
import 'package:test_screens_riverpods/domain/repository/home_repository.dart';

final listProvider = FutureProvider<List<RepositoryModel>>((ref) {
  final homeProviderState = ref.watch(homeProvider);
  return homeProviderState;
});

final homeProvider =
    StateNotifierProvider<HomeProvider, List<RepositoryModel>>((ref) {
  return HomeProvider(ref);
});

class HomeProvider extends StateNotifier<List<RepositoryModel>> {
  final Ref ref;
  List<RepositoryModel> repositories = [];
  final ScrollController scrollController = ScrollController();
  final TextEditingController searchController = TextEditingController();
  late bool isReposytoryListLoading;

  HomeProvider(this.ref) : super([]) {
    scrollController.addListener(_scrollListener);
    isReposytoryListLoading = false;
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  Future<void> searchRepositories({
    required String query,
    bool reset = false,
  }) async {
    isReposytoryListLoading = true;
    if (reset) {
      state = [];
    }
    if (query.isEmpty && reset) {
      isReposytoryListLoading = false;
      return;
    }

    if (reset) {
      state = [];
    }

    final HomeRepository homeRepository = GetIt.instance.get<HomeRepository>();

    List<RepositoryModel> updatedList =
        await homeRepository.getRepositoriesList(
      query: query,
      offset: state.length,
    );
    state = [...state, ...updatedList];
    isReposytoryListLoading = false;
  }

  void _scrollListener() {
    if (scrollController.position.atEdge) {
      if (scrollController.position.pixels == 0) {
      } else {
        searchRepositories(query: searchController.text);
      }
    }
  }

  void clearList() {
    state.clear();
  }
}
