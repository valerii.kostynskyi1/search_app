import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:test_screens_riverpods/data/model/repository_model.dart';
import 'package:test_screens_riverpods/presentation/home/providers/home_providers.dart';

class HomePage extends ConsumerWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final repositoriesData = ref.watch(listProvider);
    final homeProviderNotifier = ref.read(homeProvider.notifier);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Riverpod App'),
      ),
      body: Column(
        children: [
          TextField(
            controller: homeProviderNotifier.searchController,
            decoration: const InputDecoration(
              labelText: 'Search repositories',
            ),
          ),
          const SizedBox(height: 16.0),
          ElevatedButton(
            onPressed: () => homeProviderNotifier.searchRepositories(
                query: homeProviderNotifier.searchController.text),
            child: const Text('Search'),
          ),
          repositoriesData.when(data: (repositoriesData) {
            final List<RepositoryModel> listRepositories = repositoriesData;
            if (listRepositories.isNotEmpty) {
              return Expanded(
                child: RefreshIndicator(
                  onRefresh: () async {
                    homeProviderNotifier.clearList();
                    await homeProviderNotifier.searchRepositories(
                      query: homeProviderNotifier.searchController.text,
                      reset: true,
                    );
                  },
                  child: ListView.builder(
                    controller: homeProviderNotifier.scrollController,
                    itemCount: listRepositories.length,
                    itemBuilder: (BuildContext context, int index) {
                      final repository = listRepositories[index];
                      return ListTile(
                        title: Text(repository.name),
                        subtitle: Text(repository.description),
                        trailing: Text('${repository.stargazersCount} stars'),
                      );
                    },
                  ),
                ),
              );
            } else {
              if (homeProviderNotifier.isReposytoryListLoading) {
                return const CircularProgressIndicator();
              } else {
                return const Center(
                  child: Text(
                      'Nothing was find for your search. Please check the spelling'),
                );
              }
            }
          }, error: (err, e) {
            return Text(err.toString());
          }, loading: () {
            if (homeProviderNotifier.isReposytoryListLoading) {
              return const CircularProgressIndicator();
            } else {
              return const SizedBox();
            }
          }),
        ],
      ),
    );
  }
}
