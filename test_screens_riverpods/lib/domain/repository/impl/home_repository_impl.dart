import 'package:test_screens_riverpods/data/data_source/api_data_source.dart';
import 'package:test_screens_riverpods/data/model/repository_model.dart';
import 'package:test_screens_riverpods/domain/repository/home_repository.dart';

class HomeRepositoryImpl implements HomeRepository {
  final ApiDataSource _apiDataSource;

  HomeRepositoryImpl(
    this._apiDataSource,
  );

  @override
  Future<List<RepositoryModel>> getRepositoriesList({
    required String query,
    required int offset,
  }) {
    return _apiDataSource.getRepositoriesList(offset: offset, query: query);
  }
}
