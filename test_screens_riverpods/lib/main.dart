import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:get_it/get_it.dart';
import 'package:test_screens_riverpods/domain/repository/home_repository.dart';
import 'package:test_screens_riverpods/routes/go_router.dart';

import 'data/data_source/api_data_source.dart';
import 'data/data_source/impl/api_data_source_impl.dart';
import 'domain/repository/impl/home_repository_impl.dart';

void main() {
  GetIt.instance.registerSingleton<ApiDataSource>(ApiDataSourceImpl());
  GetIt.instance.registerSingleton<HomeRepository>(HomeRepositoryImpl(
    GetIt.instance.get<ApiDataSource>(),
  ));
  runApp(
    ProviderScope(
      child: MaterialApp.router(
        title: 'Flutter Test app',
        routerConfig: router,
      ),
    ),
  );
}
